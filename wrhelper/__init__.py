from datetime import datetime, timezone
import logging
import re


def convert_to_utc(datetime_str: str) -> str:
    iso_8601_format = '%Y-%m-%dT%H:%M:%S%z'

    normalized_datetime_str = normalize_datetime_str(datetime_str)

    if normalized_datetime_str is None:
        original_datetime = datetime.strptime(datetime_str, iso_8601_format)
    else:
        original_datetime = datetime.strptime(normalized_datetime_str, iso_8601_format)
    utc_datetime = original_datetime.astimezone(timezone.utc)

    return utc_datetime.strftime(iso_8601_format)


def normalize_datetime_str(datetime_str: str) -> str or None:
    pattern = r'([\s\S]+[+-]\d{2}):(\d{2})'

    match = re.match(pattern, datetime_str)
    if match:
        return match.group(1) + match.group(2)


def get_utc_now(datetime_format='%Y-%m-%dT%H:%M:%S%z') -> str:
    return datetime.now(timezone.utc).strftime(datetime_format)


def get_logger(log_filename, logger_name=None) -> logging.Logger:
    logging.basicConfig(
        datefmt='%Y-%m-%dT%H:%M:%S%z',
        filename=log_filename,
        format='[%(levelname)s] %(asctime)s %(name)s: %(message)s',
        level=logging.INFO
    )

    return logging.getLogger(logger_name)
